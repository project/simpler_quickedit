## INTRODUCTION

Do you want to provide users with quickedit -
but find the core quickedit really unintuitive and providing a bad experience
 (clicking on contextual link, then quick edit, then finally the field you want
 to edit)?

This modules has 3 goals:

* select which fields are quickeditable (with core it's all or none)
* allow the admin to set how to trigger the quickedit
* remove the need to give the contextual link permission


Any feedback welcome!

## REQUIREMENTS

* Quickedit core module

## RECOMMENDED MODULES

N/A

## INSTALLATION

* Install as any Drupal D8 / D9 module
* Make sure users have quickedit permissions
  
## CONFIGURATION

After the module is enabled, you will have 3 new options available when
managing your field's display. Just select the one you want to use for this
field.

The 3 options are:

* Click on the field
* Quickedit button added to each quickedit field
* Quickedit button added to each quickedit field, but only visible on hover

## SIMILAR MODULES

Quicker edit This module allows the user to quickedit through double clicks, but you would still need to give contextual link rights to that user